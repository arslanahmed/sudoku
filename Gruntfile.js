/**************************
 * @author Arslan Ahmed
 * @version 1.0
 * @created 17-07-2015
 ***************************/

module.exports = function(grunt) {

	grunt.registerTask('server', ['clean:build', 'less:style', 'concat:js', 'copy', 'uglify:js', 'connect:server', 'watch']);

	/****** CONFIGURATIONS ********/
	grunt.initConfig({
		pkg : grunt.file.readJSON('package.json'),

		// Clean distribution folder
		clean : {
			build : {
				src : ["dist/*"]
			}
		},

		// Process LESS files
		less : {
			style : {
				options : {
					mangle : false,
					banner : '/*! <%= pkg.name %> - <%= grunt.template.today("dd-mm-yyyy") %> */\n'
				},
				files : {
					"dist/resources/styles/styles.css" : "app/resources/styles/less/styles.less"
				}
			}
		},
		// Start a new build
		concat : {
			js : {
				options : {
					separator : '\n\n/************** SEPERATOR ****************/\n\n'
				},
				src : ['app/js/**/*.js', 'app/widgets/**/*.js'],
				dest : 'dist/js/built.js'
			}
		},

		// Copy files over to dist directory
		copy : {
			main : {
				files : [{
					expand : true,
					cwd : 'app/resources/images',
					src : ['**'],
					dest : 'dist/resources/images/'
				}, {
					expand : true,
					cwd : 'app/libs',
					src : ['**'],
					dest : 'dist/libs/'
				}
				/**, {
					expand : true,
					src : ['*.html'],
					dest : 'dist/'
				}**/
				]
			}
		},

		// Make it non-readable
		uglify : {
			options : {
				mangle : false,
				banner : '/*! <%= pkg.name %> - <%= grunt.template.today("dd-mm-yyyy") %> */\n'
			},
			js : {
				files : {
					'dist/js/built.min.js' : ['dist/js/built.js']
				}
			}
		},

		// Start Server
		connect : {
			server : {
				options : {
					port : 9100,
					livereload : 35729,
					hostname : 'localhost'
				}
			}
		},

		// Enable watch
		watch : {
			js : {
				files : ['*.html', 'app/js/**/*.js', 'app/js/*.js', 'app/widgets/**/*', 'Gruntfile.js'],
				tasks : ['concat:js', 'uglify:js'],
				options : {
					livereload : true,
				}
			},
			css : {
				files : ['app/resources/styles/less/*.less'],
				tasks : ['less:style'],
				options : {
					livereload : true,
				}
			}

		}
	});

	/****** DEPENDENCIES ********/
	grunt.loadNpmTasks('grunt-open');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');

};
