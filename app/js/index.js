/**************************
 * @author Arslan Ahmed
 * @version 1.0
 * @created 17-07-2015
 ***************************/

portal.controller("IndexController", ['$scope',
function($scope) {

	$scope.templates = [{
		name : 'Demo',
		url : 'app/widgets/demo/template.html'
	}];

	$scope.template = $scope.templates[0];

}]);