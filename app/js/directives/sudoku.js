/**************************
 * @author Arslan Ahmed
 * @version 1.0
 * @created 26-11-2016
 ***************************/

portal.directive('sudoku', [
function() {
	return {
		restrict : 'A',
		templateUrl : 'app/widgets/components/sudoku/template.html'
	};
}]);
