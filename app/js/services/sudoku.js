/**************************
 * @author Arslan Ahmed
 * @version 1.0
 * @created 26-11-2016
 ***************************/

portal.factory("sudoku", [
function() {

  var	DIGITS		= '123456789' ,
		ROWS 		= ['1','2','3','4','5','6','7','8','9'] ,
		COLUMNS 	= ['A','B','C','D','E','F','G','H','I'] ,
		SQUARES 	= concatenate(COLUMNS, ROWS) ,
		BLOCK_COLUMNS = ['ABC', 'DEF', 'GHI'] ,
		BLOCK_ROWS = ['123', '456', '789'] ,
	  
		UNITS_COLLECTION = [] ,		// Multi-Dimensional array consisting of rows, columns & blocks
		UNITS		= {} , 			// HashMap of squares, consisting of row, column & block arrays as values
		PEERS		= {} ;			// HashMap of squares, consisting of an array of other related squares
  
	
	// Construct Rows
	ROWS.forEach(function(row) {UNITS_COLLECTION.push(concatenate(COLUMNS, row));});
	
	// Construct Columns
	COLUMNS.forEach(function(column) {UNITS_COLLECTION.push(concatenate(column,ROWS));});
	
	// Construct Blocks
	BLOCK_COLUMNS.forEach(function(column) {
		BLOCK_ROWS.forEach(function(row) {
			UNITS_COLLECTION.push(concatenate(Array.from(column), Array.from(row)));
		});
	});

	// Build up a list of all units & it's peers
	SQUARES.forEach(function(square){
		var squarePeers = [],
			squareUnits= [];
			UNITS_COLLECTION.forEach(function(units) {
				if(units.indexOf(square) != -1){
					squareUnits.push(units);
					units.forEach(function(unit){
					if ( !(squarePeers.indexOf(unit) != -1) &&  unit !== square ) {
						squarePeers.push(unit);
					}
				});
			}
		});
		
		UNITS[square] = squareUnits;
		PEERS[square] = squarePeers;
	});
	
	
	/**
	 * Returns a new array by concatenating the objects of two arrays together
	 */
	function concatenate(a, b) {
		var result = [];
		for (var i=0; i<a.length;i++) {
			for (var j=0; j<b.length;j++) {
				result.push(a[i] + b[j]);
			}
		}
		return result;
	}
	
	function solve(board) {
		return bruteForce(parseBoard(board));
	}


	/**
	 * Initializes board with all possibilities and 
	 * proceed by elimniating digits that doesn't apply.
	 */
	function parseBoard(board) {
		
		var values = {}; 
		
		// Initially, all squares can have every possible answer from 0 to 9
		SQUARES.forEach(function(square) {
			values[square]=DIGITS;
		});

		// Report failure if a value cannot be assigned
		for (var s in board) {
			var d = board[s];
			if(d === "" || d === null ) continue;
			if (!assign(values, s, d)) {
				return false;
			}
		}
		
		return values;	
	}
	
	
	/**
	 * Depth-First search
	 */
	function bruteForce(values) {
		
		// Report failure
		if (values === false) {
			return false;
		}
		
		// Solved
		if(isPuzzleSolved(values)) {
			return values;
		}
		
		// Select the square with fewest possibilities
		var unsolvedSquares = []; 
		
		SQUARES.forEach(function(square) {
			if(values[square].length >1) unsolvedSquares.push(square);
		});
		
		unsolvedSquares.sort(function(s1,s2) { 
			if (values[s1].length != values[s2].length) {
				return values[s1].length - values[s2].length; 
			}
			
			return (s1 < s2) ? -1 : 1;

		});

		var s = unsolvedSquares[0];
		var digitsLeft = Array.from(values[s]);
		
		for (var d in digitsLeft) {
			var result = bruteForce(assign(angular.copy(values), s, digitsLeft[d]));
			if (result) return result;
		}
		
		return false;
	}


	/**
	 * Eliminate all values except d from the stack
	 */
	function assign(values, s, d) {
		var otherValues = Array.from(values[s].replace(d, ''));
		
		for (var d in otherValues) {
			if(!eliminate(values, s, otherValues[d])) {
				return false;
			}
		}
		
		return values;
	}


	/**
	 * Eliminate a digit for the provided square
	 */
	function eliminate(values, s, d) {
		
		//Already eliminated
		if (values[s].indexOf(d) == -1)	return values; 
		
		values[s] = values[s].replace(d, '');

		if (values[s].length == 0) 	return false;
		
		else if (values[s].length == 1) {
			
			var digit = values[s];
			
			// Eleminate value from it's peers 	
			for(var p in PEERS[s]) {
				if(!eliminate(values, PEERS[s][p], digit)) return false;
			}
		}
		
		
		// Loop over row, column & block in-order to see if any other square is present 
		// that can accomodate the eliminated digit (d). This algorithm works when there 
		// is just one possibility left for a square.
		
		for (var i = 0; i < UNITS[s].length; i++) {

			if (isPuzzleSolved(values)) break;

			var unit = UNITS[s][i];

			// Build a list of squares that contains digit (d).
			var dplaces = [];
			
			unit.forEach(function(u) {		
				if (values[u].indexOf(d) != -1)	dplaces.push(u);
			});

			// If a length is zero then it's an error since the retired digit needs to go somewhere
			if (dplaces.length == 0) {
				return false;
				
			} else if (dplaces.length == 1) { 			// Only one node has the possibility to keep this digit
				
				
				if (!assign(values, dplaces[0], d)) {
					return false;
				}
			}
		}

		return values;
	}
	

	function isPuzzleSolved (values) {
		for (var i = 0; i < SQUARES.length; i++) {
			if (values[SQUARES[i]].length != 1)
				return false;
		}
		return true;
	}; 


	function buildBoard (board) {
		squares = {};
		SQUARES.forEach(function(key) {
			if (board.hasOwnProperty(key))
				squares[key] = board[key];
			else
				squares[key] = '';
		});
		return squares;
	};


	return {buildBoard: buildBoard, solve : solve};
	
}]);