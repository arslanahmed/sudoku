/**************************
 * @author Arslan Ahmed
 * @version 1.0
 * @created 17-07-2015
 ***************************/

portal.factory("navigationPanel", [
function() {

	var loadPageURL = null,
	eventAlert = 'loadPage';
	
	navigationItems = [{
		name : "Company",
		icon : "fa fa-puzzle-piece fa-fw",
		url : 'app/widgets/components/sudoku/template.html'
	}, {
		name : "fa-user",
		icon : "fa fa-user fa-fw",
		url : 'app/widgets/components/developer/template.html'
	}];

	return {
		getNavigationItems : function() {
			return navigationItems;
		},
		getLoadPage : function() {
			return loadPageURL;
		},
		setLoadPage : function(url, scope) {
			loadPageURL = url;
			scope.$emit(this.getEventAlert());
		},
		getEventAlert: function(){
			return eventAlert;
		}
	};

}]);

