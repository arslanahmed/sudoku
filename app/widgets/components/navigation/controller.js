/**************************
 * @author Arslan Ahmed
 * @version 1.0
 * @created 17-07-2015
 ***************************/

portal.controller("navigation", ['$scope', 'navigationPanel',
function($scope, navigationPanel) {

	// Navigation
	$scope.navigationItems = navigationPanel.getNavigationItems();

	// Load Pages
	$scope.load = function(url) {
		navigationPanel.setLoadPage(url, $scope);
	};
	
	// Default Page (optional)
	$scope.load($scope.navigationItems[0].url);

}]);
