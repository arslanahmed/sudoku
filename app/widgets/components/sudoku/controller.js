/**************************
 * @author Arslan Ahmed
 * @version 1.0
 * @created 26-11-2016
 ***************************/

portal.controller("sudoku", ['$scope','sudoku',
function($scope, sudoku) {

$scope.solve = function() {
	$scope.squares = sudoku.solve($scope.squares);
	return $scope.squares;
};

// Construct board by filling in empty squares
var buildBoard = function(board) {
	$scope.squares = sudoku.buildBoard(board);
};

// Split Squares for grid rendering
var splitSquares = function () {
	var squares = [];
	var collection = {};
	var counter = 1;
	for(var s in $scope.squares) {
		collection[s] = $scope.squares[s];
		if(counter % 9 == 0) {
			squares.push(collection);
			collection = {};
		}
		counter++;
	}
	$scope.splitSquares = squares;
};

var initialize = function () {
	var BOARDS = [
		{A2: 4, B2: 3, B7: 1, B8: 6, C1: 5, C7: 4, C9: 8, D7: 7, D8: 2, D9: 3, E4: 2, E6: 7, E8: 4, F2: 2, F5: 4, F6: 5, F7: 8, F8: 1, F9: 6, G1: 2, G2: 1, G3: 3, G4: 4, G5: 9, H2: 9, H8: 8, I1: 4, I3: 8, I6: 3, I9: 1, A7: 5, C5: 1, B4: 7, F4: 3, C6: 9},
		{A8: 5, B1: 3, B4: 8, B6: 4, C3: 1, C5: 5, C8: 2, C9: 3, D3: 5, D4: 1, D5: 4, E2: 3, E4: 9, E6: 6, E9: 2, F3: 7, F7: 1, G2: 9, G6: 3, G8: 7, G9: 8, I2: 6, I5: 9, I6: 2},
		{A2: 1, A3: 4, A8: 5, B4: 1, C2: 7, C3: 6, C5: 5, C6: 4, C9: 1, D5: 1, D7: 4, D9: 7, E1: 2, E5: 8, E7: 3, F6: 2, G9: 5, H2: 4, H5: 3, H7: 8, H9: 6, I1: 3, I7: 7},
		{A6: 6, A7: 4, B2: 9, B7: 3, B8: 5, C2: 1, C5: 5, C9: 6, D5: 9, D8: 2, E2: 6, E4: 1, E5: 2, F3: 9, F6: 5, F7: 7, F8: 1, G1: 2, H3: 8, H5: 1, H9: 2, I4: 7, I5: 4, I7:5},
		{A2: 6, A3: 3, A5: 2, A6: 5, A8: 8, B1: 5, B4: 7, B7: 4, C3: 2, C7: 5, D6: 3, D8: 9, D9: 1, E5: 4, F4: 9, F6: 6, F9: 3, G2: 1, G5: 6, I1: 7, I3: 6, I5: 1, I7: 8},
		{A8: 2, A9: 3, B1: 5, B2: 8, B5: 7, C1: 6, C4: 5, C5: 4, D2: 2, D7: 4, E4: 6, E6: 3, E8: 5, F2: 1, F8: 7, G3: 9, G5: 5, G7: 8, H3: 1, H7: 9, I4: 4, I6: 8}
	];
	buildBoard(BOARDS[Math.floor(Math.random()*BOARDS.length)]);
	splitSquares();
};

initialize();

}]);
