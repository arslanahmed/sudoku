/**************************
 * @author Arslan Ahmed
 * @version 1.0
 * @created 17-07-2015
 ***************************/

portal.controller("developer", ['$scope',
function($scope) {

	$scope.developer = [{
		icon : "fa fa-user fa-fw",
		value : "Arslan Ahmed"
	}, {
		icon: "fa fa-phone fa-fw",
		value: "+1 (613) 700-8242"
	}, {
		icon : "fa fa-envelope-o fa-fw",
		value : "arslan@aahmed.info"
	}, {
		icon : "fa fa-globe fa-fw",
		value : "http://www.aahmed.info"
	}];
}]);

