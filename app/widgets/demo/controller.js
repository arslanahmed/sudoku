/**************************
 * @author Arslan Ahmed
 * @version 1.0
 * @created 17-07-2015
 ***************************/

portal.controller("demo", ['$scope', 'navigationPanel',
function($scope, navigationPanel) {

	var defaultPage = 'app/widgets/components/developer/template.html';
	$scope.loadPage = defaultPage;

	// Navigation Panel
	$scope.components = {
		navigation : 'app/widgets/components/navigation/template.html'
	};

	// Alert listener
	$scope.$on(navigationPanel.getEventAlert(), function() {
		if (navigationPanel.getLoadPage() !== null)
			$scope.loadPage = navigationPanel.getLoadPage();
		else
			$scope.loadPage = defaultPage;
	});
}]);
