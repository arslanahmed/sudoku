AngularJS Web Application
=================
---

#####Arslan Ahmed

✉ arslan.ahmed@live.ca 

★ http://www.aahmed.info 

☎ +1.613.700.8242

__Sudoku__
-------------------

----

**Demo** : *http://www.aahmed.info/sudoku*

#### Start Application

Execute the following (assuming npm is already installed):

   * **cd sudoku/** 
   * **npm install** # Installs all the required dependencies.
   * **grunt server** # Server should be up without any errors.
   * *Navigate to http://localhost:9100 on a browser*
   
   
#### Test Application using Jasmine & Karma

Execute the following (assuming npm is already installed):

   * **cd sudoku/** 
   * **npm install** # Installs all the required dependencies.
   * **karma start karma.conf.js** # Run testcases

#### Details

   * Comes pre-loaded with 6-boards. A random board is selected each time.
   * Test coverage is provided for all six boards in the category of solving puzzle, building boards at the service & controller level.
   * Sudoku puzzle can be loaded either by using directive **<div sudoku />** or through sudoku template (as done in this application).
   * Business logic for solving puzzle is implemented as a service.